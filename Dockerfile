FROM docker.elastic.co/elasticsearch/elasticsearch:7.15.1
WORKDIR /usr/share/elasticsearch/plugins
COPY elasticsearch-analysis-ik-7.15.1.zip .
RUN unzip elasticsearch-analysis-ik-7.15.1.zip -d ik \
    && rm elasticsearch-analysis-ik-7.15.1.zip
